import 'dart:ui';
import 'package:evrsteng_app/screens/splash_screen.dart';
import 'package:flutter/material.dart';

class OutComePredictor extends StatefulWidget {
  const OutComePredictor({Key? key}) : super(key: key);

  @override
  State<OutComePredictor> createState() => _OutComePredictorState();
}

class _OutComePredictorState extends State<OutComePredictor> {
  String _currentOutcome = '';
  String _currentCommentary = '';

  TextEditingController bowlController = TextEditingController();
  TextEditingController shotController = TextEditingController();
  TextEditingController timingController = TextEditingController();

  // Define the mapping of bowl, shot, and timing to outcome
  Map<String, Map<String, Map<String, String>>> outcomeMapping = {
    'Bouncer': {
      'Pull': {'Perfect': '6 runs', 'Early': '2 runs', 'Late': '1 wicket'},
      'UpperCut': {
        'Perfect': '6 runs',
        'Early': '1 run',
        'Good': '2 runs',
        'Late': '1 wicket'
      },
    },
    'Yorker': {
      'Straight': {'Early': '1 wicket', 'Good': '2 runs', 'Ok': '1 run'}
    },
    'Pace': {
      'Straight': {'Good': '2 runs', 'Yes': '2 runs'}
    }
  };
  // Define commentary based on outcome
  Map<String, String> commentaryMapping = {
    '6 runs': 'That’s massive and out of the ground.',
    '2 runs': 'Just over the fielder,Convert ones into twos.',
    '1 wicket': 'It’s a wicket,Excellent line and length.',
    '1 run': 'Straight to the fielder just a single',
    'Outcome not found': 'No commentary available for this outcome.'
  };

  void predictOutcome(String bowl, String shot, String timing) {
    // Fetch the outcome from the mapping
    String outcome =
        outcomeMapping[bowl]?[shot]?[timing] ?? 'Outcome not found';
    // Fetch commentary based on outcome
    String commentary = commentaryMapping[outcome] ?? '';
    setState(() {
      _currentOutcome = outcome;
      _currentCommentary = commentary;
    });
    print("commentary:: $commentary");
  }

  void refreshFields() {
    setState(() {
      bowlController.clear();
      shotController.clear();
      timingController.clear();
      _currentOutcome = '';
      _currentCommentary = '';
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.blue[900],
        title: Text(
          'CricSummit',
          style: TextStyle(
              color: Colors.white, fontWeight: FontWeight.w400, fontSize: 30),
        ),
        actions: [
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 16.0),
            child: InkWell(
                onTap: () {
                  Navigator.pushReplacement(
                      context,
                      MaterialPageRoute(
                          builder: (context) => const SplashScreen()));
                },
                child: Icon(
                  Icons.logout,
                  size: 30,
                  color: Colors.white,
                )),
          )
        ],
      ),
      body: Padding(
        padding: const EdgeInsets.all(16.0),
        child: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            mainAxisSize: MainAxisSize.min,
            children: [
              BackdropFilter(
                filter: ImageFilter.blur(
                  sigmaX: 5,
                  sigmaY: 5,
                ),
                child: Container(
                  padding:
                      EdgeInsets.only(top: 16, left: 16, right: 16, bottom: 50),
                  decoration: BoxDecoration(
                      color: Colors.green.withOpacity(.15),
                      borderRadius: BorderRadius.circular(18.0)),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.stretch,
                    children: [
                      TextField(
                        controller: bowlController,
                        decoration: const InputDecoration(
                            labelText: 'Enter bowl type',
                            labelStyle: TextStyle(color: Colors.deepPurple)),
                        onChanged: (value) {
                          // Handle bowl type change
                        },
                      ),
                      TextField(
                        controller: shotController,
                        decoration: const InputDecoration(
                            labelText: 'Enter shot type',
                            labelStyle: TextStyle(color: Colors.deepPurple)),
                        onChanged: (value) {
                          // Handle shot type change
                        },
                      ),
                      TextField(
                        controller: timingController,
                        decoration: const InputDecoration(
                            labelText: 'Enter shot timing',
                            labelStyle: TextStyle(color: Colors.deepPurple)),
                        onChanged: (value) {
                          // Handle shot timing change
                        },
                      ),
                    ],
                  ),
                ),
              ),
              SizedBox(height: 30),
              Row(
                children: [
                  Expanded(
                    child: InkWell(
                      onTap: () {
                        refreshFields();
                      },
                      child: Container(
                        height: 40,
                        alignment: Alignment.center,
                        decoration: BoxDecoration(
                            // color: Colors.purple,
                            borderRadius: BorderRadius.circular(22.0)),
                        child: Text(
                          "Refresh",
                          style: TextStyle(
                              fontSize: 16,
                              color: Colors.white,
                              fontWeight: FontWeight.w500),
                        ),
                      ),
                    ),
                  ),
                  SizedBox(
                    width: 10,
                  ),
                  Expanded(
                    child: InkWell(
                      onTap: () {
                        // Check if any field is empty
                        if (bowlController.text.isEmpty ||
                            shotController.text.isEmpty ||
                            timingController.text.isEmpty) {
                          // Show a dialog or a snackbar indicating the error
                          ScaffoldMessenger.of(context).showSnackBar(SnackBar(
                            content: Text('Please fill all fields.'),
                          ));
                        } else {
                          // Predict the outcome
                          predictOutcome(
                            bowlController.text.toString(),
                            shotController.text.toString(),
                            timingController.text.toString(),
                          );
                        }
                        bowlController.clear();
                        shotController.clear();
                        timingController.clear();
                      },
                      child: Container(
                          height: 40,
                          alignment: Alignment.center,
                          decoration: BoxDecoration(
                              color: Colors.blue[900],
                              borderRadius: BorderRadius.circular(22.0)),
                          child: Text(
                            'Submit',
                            style: TextStyle(
                                fontSize: 16,
                                color: Colors.white,
                                fontWeight: FontWeight.w500),
                          )),
                    ),
                  ),
                ],
              ),
              SizedBox(height: 30),
              Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.start,
                children: [
                  RichText(
                    textAlign: TextAlign.start,
                    text: TextSpan(
                      text: 'Outcome: ',
                      style: TextStyle(
                          fontSize: 20,
                          color: Colors.green,
                          fontWeight:
                              FontWeight.w500), // You can adjust the style here
                      children: <TextSpan>[
                        TextSpan(
                          text: '$_currentOutcome',
                          style: TextStyle(
                            fontSize: 16,
                            color: Colors.black,
                          ),
                        ),
                      ],
                    ),
                  ),
                  SizedBox(height: 10),
                  RichText(
                    textAlign: TextAlign.start,
                    text: TextSpan(
                      text: 'Commentary: ',
                      style: TextStyle(
                          fontSize: 20,
                          color: Colors.red,
                          fontWeight:
                              FontWeight.w500), // You can adjust the style here
                      children: <TextSpan>[
                        TextSpan(
                          text: '$_currentCommentary',
                          style: TextStyle(fontSize: 16, color: Colors.black),
                        ),
                      ],
                    ),
                  ),
                ],
              )
            ],
          ),
        ),
      ),
    );
  }
}
