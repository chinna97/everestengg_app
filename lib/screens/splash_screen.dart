import 'dart:async';
import 'package:flutter/material.dart';
import 'package:lottie/lottie.dart';
import 'outComePredictor_screen.dart';

class SplashScreen extends StatefulWidget {
  const SplashScreen({Key? key}) : super(key: key);

  @override
  State<SplashScreen> createState() => _SplashScreenState();
}

class _SplashScreenState extends State<SplashScreen> {

  @override
  void initState() {
    super.initState();
    Timer(const Duration(seconds: 3), () =>
        Navigator.pushReplacement(context,
            MaterialPageRoute(builder: (context) => const OutComePredictor())));
  }

  @override
  Widget build(BuildContext context) {
    return  Scaffold(
      backgroundColor: Colors.white,
      body: Padding(
        padding: const EdgeInsets.symmetric(vertical: 4.0, horizontal: 12),
        child: Center(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: [
              Lottie.asset(
                'assets/json/cricket.json', // Replace with the path to your Lottie animation file
                width: 300,
                height: 300,
                fit: BoxFit.cover,
                repeat: true, // Set to true if you want the animation to loop
              ),
              Text("CricSummit 2021...",style: TextStyle(color: Colors.red[900],fontSize: 35,fontStyle: FontStyle.italic),)
            ],
          ),
        ),
      ),
    );
  }
}
